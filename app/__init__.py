from flask import Flask
from config import Config
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

from app.router.cust_router import *
from app.router.borrow_router import *


app.register_blueprint(cust_blueprint)
app.register_blueprint(rental_blueprint)