from app.model.borrows_model import database, showRentalById, insertNewRental, updateRentalById, showRentalByEmail
from app.model.cust_model import database as cust_db, showMemberByEmail
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()
cust_db = cust_db()

@jwt_required()
def showPerRentalId(**params):
    params = request.json
    dbresult = showRentalById(**params)
    result = []
    for item in dbresult:
        rent_list = {
            "rentalid" : item["rentalid"],
            "rentdate" : item["rentdate"],
            "filmid" : item["filmid"],
            "rentstat" : item["rentstat"] 
        }
        result.append(rent_list)
    return jsonify(result)

@jwt_required()
def showPerRentalEmail(**params):
    params = get_jwt_identity()
    dbresult = showRentalByEmail(**params)
    result = []
    if dbresult is not None:
        for item in dbresult:
            rentalid = json.dumps({"rentalid":item['rentalid']})
            dvdetail = getDvdById(rentalid)
            rent_list = {
            "rentalid" : item["rentalid"],
            "rentdate" : item["rentdate"],
            "filmid" : item["filmid"],
            "rentstat" : item["rentstat"],
            "title" : dvdetail["title"],
            "genre" : dvdetail["genre"],
            "country" : dvdetail["country"] 
        }

def getDvdById(data):
    dvd_data = request.get(url="http:/localhost:8000/dvdbyid", data=data)
    return dvd_data.json()

@jwt_required()
def insertNewRent(**params):
    token = get_jwt_identity()
    memberid = showMemberByEmail(**token)["memberid"]
    rentaldate = datetime.datetime.now().isoformat()
    filmid = json.dumps({"id":params["filmid"]})
    title = getDvdById(filmid)["title"]
    params.update(
        {
            "memberid" : memberid,
            "rentaldate": rentaldate,
            "title" : title,
            "rentstat" : 1
        }
    )
    insertNewRental(**params)
    return jsonify({"massage": "Succcess"})


@jwt_required()
def updateStat(**params):
    params = request.json
    dbresult = updateRentalById(**params)
    return jsonify({'massage':"successfull"})