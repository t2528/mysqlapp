from app.model.cust_model import database, showMember, showMemberById, insertNewMember, deleteMemberById, updateMemberById, showMemberByEmail
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()

def shows():
    dbresult = showMember()
    result = []
    for items in dbresult:
        member = {
            "memberid" : items["memberid"],
            "username" : items["username"],
            "firstname" : items["firstname"],
            "lastname" : items["lastname"],
            "email" : items["email"] 
        }
        result.append(member)
    return jsonify(result)


#route untuk melihat member berdasarkan id
def showPerMember(**params):
    params = request.json
    dbresult = showMemberById(**params)
    member = {
            "memberid" : dbresult["memberid"],
            "username" : dbresult["username"],
            "firstname" : dbresult["firstname"],
            "lastname" : dbresult["lastname"],
            "email" : dbresult["email"] 
        }
    return jsonify(member) 

#route untuk menambah peminjaman
def insertNewRent(**params):
    params = request.json
    dbresult = insertNewMember(**params)
    return jsonify({'massage':"successfull"})

def updateStat(**params):
    params = request.json
    dbresult = updateMemberById(**params)
    return jsonify({'massage':"successfull"})

def deleteMember(**params):
    params = request.json
    dbresult = deleteMemberById(**params)
    return jsonify({'massage':"successfull"})

def token (**params):
    params = request.json
    dbresult = showMemberByEmail(**params)
    if dbresult is not None:
        member = {
            "username" : dbresult["username"],
            "email" : dbresult["email"]
        }

        expires = datetime.timedelta(days=1)
        acces_token = create_access_token (member, fresh=True, expires_delta=expires)

        data = {
            "data" : member,
            "token_access" : acces_token
        }
    else:
        data = {
            "massage" : "Email tidak terdaftar"
        }
    return jsonify(data)

