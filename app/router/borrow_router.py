from app import app
from app.controller import borrow_controller
from flask import Blueprint, request

rental_blueprint = Blueprint("rental_router",__name__)

@app.route("/rental", methods = ['GET'])
def routeShowRental():
    return borrow_controller.showsPerRentalId()

@app.route("/updatestat", methods = ['POST'])
def routeUpdateStat():
    params = request.json
    return borrow_controller.updateStat()

@app.route("/addrent", methods = ['POST'])
def routeInsertRent():
    params = request.json
    return borrow_controller.insertNewRent()