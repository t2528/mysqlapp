import json
from app import app
from app.controller import cust_controller
from flask import Blueprint, request

cust_blueprint = Blueprint("cust_router",__name__)

@app.route("/members", methods = ['GET'])
def routeShowMembers():
    return cust_controller.shows()

@app.route("/member", methods = ['GET'])
def routeShowMember():
    params = request.json
    return cust_controller.showPerMember()

@app.route("/delmember", methods = ['POST'])
def routeDeleteMember():
    params = request.json
    return cust_controller.deleteMember()

@app.route("/requesttoken", methods = ['POST'])
def routeRequestToken():
    params = request.json
    return cust_controller.token()