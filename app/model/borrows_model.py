from mysql.connector import connect
from sqlalchemy import exc, delete, update
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.sqltypes import DATE, Boolean

Base = declarative_base()

class database():
   def __init__(self):
      engine = create_engine('mysql+mysqlconnector://root:12345678@localhost:3306/tugas_akhir', echo = True)
      Session = sessionmaker(bind=engine)
      self.session = Session()

class Rental(Base):
    __tablename__ = 'rental'
    rentalid = Column(Integer, primary_key = True)
    memberid = Column(Integer)
    rentdate = Column(DATE)
    filmid = Column(String)
    rentstat = Column(Boolean)

db = database()

#melihat daftar peminjaman dvd member tertentu
def showRentalById(**params):
    resultdb = db.session.query(Rental).filter(Rental.memberid==params['memberid']).all()
    # result = row2dict(resultdb)
    result = []
    for row in resultdb:
        rentById = {"rentalid":row.rentalid,"rentdate":row.rentdate,"filmid":row.filmid,"rentstat":row.rentstat}
        result.append(rentById)
    return result

def showRentalByEmail(**params):
    resultdb = db.session.query(Rental).filter(Rental.memberid==params['memberid']).all()
    result = []
    for row in resultdb:
        rentById = {"rentalid":row.rentalid,"rentdate":row.rentdate,"filmid":row.filmid,"rentstat":row.rentstat}
        result.append(rentById)
    return result

#fungsi untuk menambahkan daftar peminjaman
def insertNewRental(**params):
    db.session.add(Rental(**params))
    result = db.session.commit()
    return result

#fungsi untuk mengubah status peminjaman
def updateRentalById(**params):
    resultdb = db.session.query(Rental).filter(Rental.rentalid==params['rentalid']).one()
    resultdb.rentstat = params['rentstat']
    db.session.commit()

gantiStat = {
                "rentalid" : 6,
                "rentstat": 1
            }

tambahRental = {
                "rentalid" : 8,
                "memberid" : 2,
                "rentdate" : "2021-09-12",
                "filmid" : 'W1',
                "rentstat": 1,
            }

# showRentalById(**memberId)
# insertNewRental(**tambahRental)
# updateRentalById(**gantiStat)