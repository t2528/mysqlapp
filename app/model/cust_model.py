from mysql.connector import connect
from sqlalchemy import exc, delete, update
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class database():
   def __init__(self):
      engine = create_engine('mysql+mysqlconnector://root:12345678@localhost:3306/tugas_akhir', echo = True)
      Session = sessionmaker(bind=engine)
      self.session = Session()

class Member(Base):
   __tablename__ = 'members'
   memberid = Column(Integer, primary_key =  True)
   username = Column(String)
   firstname = Column(String)
   lastname = Column(String)
   email = Column(String)

db = database()

#fungsi untuk mengubah menjadi dict
def row2dict(row):
    d={}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))
    return d

#fungsi untuk melihat member
def showMember ():
    resultdb =  db.session.query(Member).all()
    result = [row2dict(row) for row in resultdb]
    return result

#fungsi untuk melihat member berdasarkan id
def showMemberById(**params):
    resultdb = db.session.query(Member).filter(Member.memberid==params['memberid']).one()
    result = row2dict(resultdb)
    return result

#fungsi untuk melihat member berdasarkan email
def showMemberByEmail(**params):
    resultdb = db.session.query(Member).filter(Member.email==params['email']).one()
    result = {"memberid":resultdb.memberid,"username":resultdb.username,"email":resultdb.email}
    return result

#fungsi untuk menambah member baru
def insertNewMember(**params):
    try: 
        db.session.add(Member(**params))
        return db.session.commit()
    except exc.IntegrityError as e:
        db.session.rollback()
        print("Data sudah ada dalam basis data: {}".format(e))

#fungsi untuk mengubah keterangan member
def updateMemberById(**params):
    resultdb = db.session.query(Member).filter(Member.memberid==params['memberid'])
    resultdb.update(params)
    db.session.commit()

#fungsi untuk menghapus member
def deleteMemberById(**params):
    resultdb = db.session.query(Member).filter(Member.memberid==params['memberid'])
    resultdb.delete()
    db.session.commit()


memberId = {
                "memberid" : "1",
                "username" : "arifira",
                "firstname" : "Arif",
                "lastname": "Fira",
                "email": "arifira2@gmail.com"
            }

tambahMember = {
                "memberid" : "6",
                "username" : "firdaa",
                "firstname" : "Firda",
                "lastname": "Ardi",
                "email": "firdardi@gmail.com"
            }


# showMember()
# showMemberById(**memberId)
# insertNewMember(**tambahMember)
# updateMemberById(**memberId)
# deleteMemberById(**tambahMember)





