RENTAL_DVD
Merupakan aplikasi pencatatan kegiatan rental dvd berbagai film dan series oleh member yang sudah terdaftar ataupun yang baru
mendaftar.

Pembuatan ini mengikuti tutorial yang diberikan, dengan ada sedikit perbedaan pada kegunaan dan database aplikasi. Selain itu, 
pengembangan ada pada aplikasi ini menggunakan yang ORM/ODM. Aplikasi ini belum selesai untuk dibuat dikarenakan adanya keterbatasan
pada pembuatan pembuatan microservice yang menggunakan MongoDB.

Fungsi yang tersedia pada aplikasi:

CUSTOMERS
- melihat member
- melihat member berdasarkan id
- memasukkan data member baru
- mengubah data member
- menghapus data member

RENTAL
- melihat daftar peminjaman dvd member tertentu
- menambahkan daftar peminjaman
- mengubah status peminjaman

FILM
- melihat semua dvd
- melihat dvd dengan nama
- melihat dvd berdasarkan genre
- menambahkan dvd
- mengubah data dvd
- menghapus data dvd

Terdapat beberapa REST API yang tersedia, yaitu:
- untuk melihat data keseluruhan member, dengan mengakses pada postman: "http:\localhost:5000\members"
- untuk meliaht data berdasarkan id member tertentu, dengan mengakses pada postman: "http:\localhost:5000\member"
- untuk menghapus data member berdasarkan id member tertentu, dengan mengakses pada postman: "http:\localhost:5000\delmember"
- untuk meminta akses token dengan email member tertentu, dengan mengakses pada postman: "http:\localhost:5000\requesttoken"
- untuk menunjukkan riwayat pemesanan dengan menggunakan email member tertentu, dengan mengakses pada postman: "http:\localhost:5000\rental"
- untuk mengganti status peminjaman suatu pemesanan, dengan mengakses pada postman: "http:\localhost:5000\updatestat"
- untuk menambah peminjaman, dengan mengkases pada postman: "http:\localhost:5000\addrent"
- 



